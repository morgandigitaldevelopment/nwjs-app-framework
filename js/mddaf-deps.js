//Globals
var _LOC = "./",
	_VIEWLOC = "./views/",
	_ANGULAR = {},
	$ang,
	_jQ,
	_v;

//Load Deps

var deps = [
	_LOC+"lib/js/jquery.min.js",
	_LOC+"lib/js/angular.min.js",
	_LOC+"lib/js/mustache.min.js",
	_LOC+"lib/js/underscore.js",
	_LOC+"js/mddaf.js",
	_LOC+"js/mddaf-router.js",
	_LOC+"js/mddaf-sidemenu.js"
]

head.load(_LOC+"lib/js/velocity.min.js", function() {
    // Call a function when done
    _v = Velocity;
});

head.load(deps, function() {
	 head.load(_LOC+"lib/js/velocity.min.js");
    // Call a function when done
    _jQ = jQuery.noConflict();
    head.load();

	//load promise polyfil if needed
    if(typeof(Promise) === "undefined"){
		head.load(_LOC+"lib/js/promise.js");
    }

	$ang = angular.module('MDDAF',[]);
	$ang.config(function($interpolateProvider){
		$interpolateProvider.startSymbol('[[').endSymbol(']]');
	});
	$ang.controller("sideMenuCtrl",MDDAF._SIDEMENU.ctrl);

   //load helper js methods
   head.load(_LOC+"js/helpers.js",function(){

   });

   //load inidicator controller
   head.load(_LOC+"js/indicator.js",function(){
	   MDDAF.$indicator.init();
   });

   head.ready(function(){

   })

});
