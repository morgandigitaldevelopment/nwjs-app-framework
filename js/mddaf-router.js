(function(){
    MDDAF.Router = {
        viewCache:{
            raw:{},
            rendered:{}
        },
        nav:{
            go2:function(view){

                var $router = MDDAF.Router
                    $sidemenu = _ANGULAR.sidemenu;

                //load html view with mustache
                $router.getViewHTML(view,function(data){
                    $router.updateContent(data);
                    $sidemenu.hide();
                    $sidemenu.$apply();
                });
            }
        },
        loadView:function(view){

            //return function width
            return function(viewJS,cb){
                head.load(viewJS,cb);
            }
        },
        updateContent:function(renderedView){
            //get content container
            var content = document.getElementById('content');
                content.innerHTML = renderedView;
                MDDAF.$indicator.hide();
        },
        renderContent:function(data){

        },
        getViewHTML:function(view,cb){

            var good = function(data){
                MDDAF.Router.viewCache.raw[view] = data;
                cb(data);
                // MDDAF.$indicator.hide();
            },
            bad = function(){
                alert('view load failed');
                MDDAF.$indicator.hide();
            }
            _jQ.ajax({
    		  url: _VIEWLOC+view+".html",
    		  dataType: 'html',
    		  success: good,
    		  error: bad
    		});
        }
    }
})();
