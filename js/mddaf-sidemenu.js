MDDAF._SIDEMENU = {
    ctrl:function($scope){
        $scope.element = function(){
            return document.getElementById('sidemenu');
        };
        $scope.menu = {
            items:[
                {id:1,label:'Splash',view:'post-splash'},
                {id:2,label:'Init',view:'init'}
            ]
        }
        $scope.nav = function(view){

            MDDAF.$indicator.loading(function(){
                MDDAF.Router.nav.go2(view);
            });

        }
        //check initial visibility
        $scope.visible = !(_jQ($scope.element()).css("left").split('px')[0] < 0);

        //show menu
        $scope.show = function(){
            if($scope.visible != true){
                // _v(MDDAF.appContainer(),{left:_jQ($scope.element()).width()},{duration:350});
                _v($scope.element(),{left:'0%'},{duration:400});
                $scope.visible = true;
            }
        }

        //hide menu
        $scope.hide = function(){
            if($scope.visible == true){
                _v($scope.element(),{left:'-80%'},{duration:300});
                // _v(MDDAF.appContainer(),{left:'0%'},{duration:400});
                $scope.visible = false;
            }
        }

        //pass reference
        _ANGULAR.sidemenu = $scope;
    }
}
